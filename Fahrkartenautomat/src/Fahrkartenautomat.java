﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag; 
       double eingezahlterGesamtbetrag;
       double eingeworfeneMünze;
       double rückgabebetrag;
       int anzahlTickets;
       
       // Ticketinformation
       
       System.out.println("Wählen Sie ihre Fahrkarte für Berlin AB aus: \n"  );
		System.out.println("Auswahlnummer		Bezeichnung					   Preis in Euro");

		System.out.println("1			Einzelfahrschein Berlin AB				2,90");
		System.out.println("2 			Einzelfahrschein Berlin BC				3,30");
		System.out.println("3			Einzelfahrschein Berlin ABC				3,60");
		System.out.println("4			Kurzstrecke						1,90");
		System.out.println("5			Tageskarte Berlin AB					8,60");
		System.out.println("6			Tageskarte Berlin BC					9,00");
		System.out.println("7			Tageskarte Berlin ABC					9,60");
		System.out.println("8			Kleingruppen-Tageskarte Berlin AB			23,50");
		System.out.println("9			Kleingruppen-Tageskarte Berlin BC			24,30");
		System.out.println("10			Kleingruppen-Tageskarte Berlin ABC			24,90");
       

		
		System.out.print("Anzahl der Tickets: ");
	   anzahlTickets = tastatur.nextInt();
	   
	   anzahlTickets
	   
       System.out.print("Zu zahlender Betrag (EURO): ");
       zuZahlenderBetrag = tastatur.nextDouble();
       
       
       
       
       
       // Geldeinwurf
       // -----------
       eingezahlterGesamtbetrag = 0.0;
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
       {
    	   System.out.printf("Noch zu zahlen: %.2f Euro %n", ((zuZahlenderBetrag * anzahlTickets) - eingezahlterGesamtbetrag));
    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
    	   eingeworfeneMünze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
       }

       // Fahrscheinausgabe
       // -----------------
       System.out.println("\nFahrschein wird ausgegeben");
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");

       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
       if(rückgabebetrag > 0.0)
       {
    	   System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
           {
        	  System.out.println("2 EURO");
	          rückgabebetrag -= 2.0;
           }
           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
           {
        	  System.out.println("1 EURO");
	          rückgabebetrag -= 1.0;
           }
           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
           {
        	  System.out.println("50 CENT");
	          rückgabebetrag -= 0.5;
           }
           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
           {
        	  System.out.println("20 CENT");
 	          rückgabebetrag -= 0.2;
           }
           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
           {
        	  System.out.println("10 CENT");
	          rückgabebetrag -= 0.1;
           }
           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
           {
        	  System.out.println("5 CENT");
 	          rückgabebetrag -= 0.05;
           }
       }

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
       
       tastatur.close();
      
    }
}